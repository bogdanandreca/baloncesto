import java.util.*;
public class Jugador{
    private String nombreJugador;
    private int votos;
    public Jugador(String nombreJugador,int votos){
        this.nombreJugador=nombreJugador;
        this.votos=votos;
    }

    public String getNombreJugador() {
        return nombreJugador;
    }

    public void setNombreJugador(String nombreJugador) {
        this.nombreJugador = nombreJugador;
    }

    public int getVotos() {
        return votos;
    }

    public void setVotos(int votos) {
        this.votos = votos;
    }
}