
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
public class Acb extends HttpServlet {

    private ModeloDatos bd;

    public void init(ServletConfig cfg) throws ServletException {
        bd = new ModeloDatos();
        bd.abrirConexion();
    }

    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        HttpSession s = req.getSession(true);
        String nombreP = (String) req.getParameter("txtNombre");
        String nombre = (String) req.getParameter("R1");
        Map<String,Integer> jugadores=new HashMap<String,Integer>();
        jugadores = bd.extraerDatosJugadores();
        s.setAttribute("jugadores", jugadores);
        if(req.getParameter("button1")!=null){
            bd.borrarVotos();
            res.sendRedirect(res.encodeRedirectURL("VotosACero.jsp"));
        }
        else if(req.getParameter("button2")!=null){
            res.sendRedirect(res.encodeRedirectURL("VerVotos.jsp"));
        }else{
            if (nombre.equals("Otros")) {
                nombre = (String) req.getParameter("txtOtros");
            }
            if (bd.existeJugador(nombre)) {
                bd.actualizarJugador(nombre);
            } else {
                bd.insertarJugador(nombre);
            }
            s.setAttribute("nombreCliente", nombreP);
            // Llamada a la página jsp que nos da las gracias
            res.sendRedirect(res.encodeRedirectURL("TablaVotos.jsp"));
        }
    }

    public void destroy() {
        bd.cerrarConexion();
        super.destroy();
    }
}
