import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import java.util.List;

public class PruebasPhantomjsIT
{
private static WebDriver driver=null;
@Test
public void tituloIndexTest()
{
DesiredCapabilities caps = new DesiredCapabilities();
caps.setJavascriptEnabled(true);
caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});
driver = new PhantomJSDriver(caps);
driver.navigate().to("http://localhost:8080/Baloncesto/");
assertEquals("Votacion mejor jugador liga ACB", driver.getTitle(), "El titulo no es correcto");
System.out.println(driver.getTitle());
driver.close();
driver.quit();
}

@Test
public void PFA()
{
DesiredCapabilities caps = new DesiredCapabilities();
caps.setJavascriptEnabled(true);
caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});
driver = new PhantomJSDriver(caps);
driver.navigate().to("http://localhost:8080/Baloncesto/");
driver.findElement(By.id("Votoscero")).click();
driver.findElement(By.xpath("//a[@href='index.html']")).click();
driver.findElement(By.id("Vervotos")).click();
WebElement tbl = driver.findElement(By.id("tableJugadores"));
List<WebElement> rows = tbl.findElements(By.tagName("tr"));
boolean expResult = true;
boolean result = true;
for (WebElement row : rows) {
    List<WebElement> td = row.findElements(By.tagName("td"));
    if (td.size() > 0) {
        if(td.get(1).getText().equals("1")){
            result=false;
        }
    }
}

assertEquals(expResult, result, "Todos los votos estan a 0");
driver.close();
driver.quit();
}


@Test
public void PFB()
{
DesiredCapabilities caps = new DesiredCapabilities();
caps.setJavascriptEnabled(true);
caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});
driver = new PhantomJSDriver(caps);
driver.navigate().to("http://localhost:8080/Baloncesto/");
driver.findElement(By.id("OtrosCheck")).click();
driver.findElement(By.id("Otrostext")).sendKeys("Bogdan");
driver.findElement(By.id("Votartext")).click();
driver.findElement(By.xpath("//a[@href='index.html']")).click();
driver.findElement(By.id("Vervotos")).click();
WebElement tbl = driver.findElement(By.id("tableJugadores"));
List<WebElement> rows = tbl.findElements(By.tagName("tr"));
boolean expResult = true;
boolean result=false;
for (WebElement row : rows) {
    List<WebElement> td = row.findElements(By.tagName("td"));
    if (td.size() > 0) {
        if(td.get(0).getText().equals("Bogdan")){
            if(td.get(1).getText().equals("1")){
                result=true;
            }
        }
    }
}

assertEquals(expResult, result, "El jugador Bogdan tiene un voto");
driver.close();
driver.quit();
}
}

